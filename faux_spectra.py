# %load faux_spectra.py

import functools

class FauxSpectra(object):
    def __init__(self, scans=1000, rt=None, minmz=300, maxmz=1600, num_peptides=20,
                 noisiness=1.0, isotope_noisiness=1.0):
        self.scans             = scans
        self.rt                = rt
        self.minmz             = minmz
        self.maxmz             = maxmz
        self.num_peptides      = num_peptides
        self.noisiness         = noisiness
        self.isotope_noisiness = isotope_noisiness
        
        self._base_intensity_dist = self.truncated_normal(low=0.0, mean=1000, upp=10e7)
        self._isotopes_noise_dist = self.truncated_normal(low=1.0 - self.isotope_noisiness, mean=1.0, upp=1.0 + self.isotope_noisiness, sd=self.isotope_noisiness / 3)
        self._num_isotopes_dist   = self._generate_func_from_curve_fit()
        self._charge_dist         = self.truncated_normal(mean=2.0, low=0.0, upp=EXPECTED_MZ_DIFFS.size)
        self._noise_mz_dist       = self.squared_rvs(self.minmz, self.maxmz)
        self._noise_intens_dist   = self.truncated_normal(low=0.0, mean=10, upp=100)
        
    @staticmethod
    def truncated_normal(mean=0, sd=1, low=0, upp=10):
        return scipy.stats.truncnorm((low - mean) / sd, (upp - mean) / sd, loc=mean, scale=sd)
    
    @staticmethod
    def squared_rvs(minval, maxval):
        partial = functools.partial(scipy.stats.uniform.rvs, loc=np.sqrt(minval),
                                    scale=np.sqrt(maxval) - np.sqrt(minval))
        return lambda *args, **kwargs: partial(*args, **kwargs) ** 2

    @staticmethod
    def random_between(min, max):
        return (np.random.random() * (max - min)) + min
    
    def generate(self):
        if self.rt is None:
            rts   = np.linspace(0.01, 0.01 * self.scans, self.scans)
            scans = np.arange(0, self.scans + 1)
        else:
            rts   = np.arange(0.01, self.rt, 0.01)
            scans = np.arange(0, rts.size + 1)
        return [self.generate_scan(idx, rt) for idx, rt in zip(scans, rts)]
    
    def generate_scan(self, index, rt):
        mzs, intenss = self.generate_mzs_intenss()
        return { 'index': index, 'id': index, 'rt': rt, 'number': index, 'nativeId': index,
                 'mz': mzs, 'intens': intenss }

    def generate_mzs_intenss(self):
        mzs, intenss = [], []
        for _ in range(self.random_num_peptides()):
            mz_coord       = self.random_mz_coordinate()
            charge_delta   = self.random_charge_delta(mz_coord)
            isotope_coords = self.random_isotopes(mz_coord, charge_delta)
            # display(f'charge: {charge_delta}; isotope coords: {printnp(isotope_coords)}; deltas: {printnp(np.diff(isotope_coords))}')
            intensities    = self.random_isotope_intensities(len(isotope_coords))
            mzs.extend(isotope_coords)
            intenss.extend(intensities)

        noise_mzs, noise_intenss = self.random_noise_peaks(len(mzs))
        # display(f'noise mzs: {noise_mzs.size}')
        mzs.extend(noise_mzs)
        intenss.extend(noise_intenss)

        mzs       = np.array(mzs)
        intenss   = np.array(intenss)
        sort_idxs = np.argsort(mzs)
        return mzs[sort_idxs], intenss[sort_idxs]

    def random_mz_coordinate(self, ):
        return self.random_between(self.minmz, self.maxmz)

    def random_base_intensity(self, ):
        return self._base_intensity_dist.rvs()

    def random_within_perc_range(self, value, lower_perc, upper_perc):
        return self.random_between(value * lower_perc, value * upper_perc)

    def random_charge_delta(self, mz_coord):
        charge = int(round(self._charge_dist.rvs()))
        return EXPECTED_MZ_DIFFS[charge]

    def random_isotope_intensities(self, num_isotopes):
        base_intens = self.random_base_intensity()
        intensities = [base_intens * self.random_between(0.8, 1.2)]
        for i in range(1, num_isotopes):
            intensities.append(intensities[i - 1] * self.random_between(0.4, 0.9))
        return intensities

    def random_noise_peaks(self, num_isotope_points):
        mzs     = self._noise_mz_dist(size=int(self.noisiness * num_isotope_points))
        intenss = self._noise_intens_dist.rvs(mzs.size)
        return mzs, intenss

    def random_num_peptides(self):
        return int(round(
            self.truncated_normal(mean=self.num_peptides, low=self.num_peptides * 0.5,
                                  upp=self.num_peptides * 1.5, sd=self.num_peptides // 3).rvs()))
    
    def random_isotopes(self, mz_coord, charge_delta):
        num_isotopes = self._num_isotopes_dist(mz_coord)
        return [
            (mz_coord + (i * charge_delta * self._isotopes_noise_dist.rvs()))
            for i in range(num_isotopes)
        ]
    
    def _generate_func_from_curve_fit(self, mzs=[300, 700, 1800], num_isotopes=[3, 4, 8]):
        quad = lambda x, a, b, c: a * x ** 2 + x * b + c
        popt, pcov = scipy.optimize.curve_fit(quad, mzs, num_isotopes)
        def _func(mz_coord):
            avg_iso = quad(mz_coord, *popt)
            avg_iso = truncated_normal(mean=avg_iso, low=0.3 * avg_iso, upp=2.0 * avg_iso).rvs()
            return int(round(avg_iso))
        return _func
