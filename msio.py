# %load io.py
import numpy as np


def read_mzml_file(filename, mslevel=None, verbose=False):
    from pyteomics import mzml
    
    def _parse_scan(index, scan):
        return {
            'index':    index,
            'id':       scan['index'],
            'rt':       float(scan['scanList']['scan'][0]['scan start time']),
            'number':   scan['index'],
            'nativeId': scan['id'],
            'mz':       scan['m/z array'],
            'intens':   scan['intensity array']
        }
    
    spectra = mzml.read(filename, use_index=True)
    spectra = [
        _parse_scan(index, scan)
        for index, scan in enumerate(spectra)
        if mslevel is None or scan.get('ms level') == mslevel
    ]
    if verbose:
        display('# scans', len(spectra))
        display('sample spectra: {}'.format(spectra[len(spectra // 2)]))
        
    return spectra
        

def read_byspec2_file(filename, orderby=None, offset=None, limit=None, mslevel=None, verbose=False):
    import sqlite3
    def _parse_sqlite_results(results):
        return [{
            'index':    index,
            'id':       row[0],
            'rt':       row[1],
            'number':   row[2],
            'nativeId': row[3],
            'mz':       np.frombuffer(row[4], dtype=np.float64),
            'intens':   np.frombuffer(row[5], dtype=np.float32)
        } for index, row in enumerate(results)]
    
    level_filter = '(s.MSLevel = ?) '     if mslevel is not None else ''
    limit_filter = 'LIMIT ?,? '           if offset  is not None and limit is not None else ''
    order_filter = f'ORDER BY {orderby} ' if orderby else ''
    params       = list(filter(lambda x: x is not None, [mslevel, offset, limit * 2, offset, limit]))
    
    connection   = sqlite3.connect(filename)
    cursor       = connection.cursor()
    query        = 'SELECT s.Id, s.RetentionTime, s.ScanNumber, s.NativeId, p.PeaksMz, p.PeaksIntensity ' + \
                   'FROM Spectra AS s JOIN Peaks AS p ON (s.PeaksId = p.Id) ' + \
                   'WHERE s.Id IN (' + \
                       f'Select s.Id FROM Spectra AS s WHERE {level_filter}{order_filter}{limit_filter}' + \
                  f') AND (p.PeaksCount > 10) {limit_filter}'
    
    if verbose:
        display(f'INFO | full query: {query}; params: {params}')
    
    parsed_scans = _parse_sqlite_results(cursor.execute(query, params).fetchall())
    connection.close()
    return parsed_scans


def printnp(arr):
    return ', '.join(map(lambda x: '{:.4f}'.format(x), arr))
