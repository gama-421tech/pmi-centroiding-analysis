import numpy as np
import scipy
import scipy.signal

def convert_to_uniform_grid(scan, resolution=1000, method='linear'):
    mzS    = np.floor(scan['mz'][0])
    mzE    = np.ceil(scan['mz'][-1])
    grid   = np.linspace(mzS, mzE, int((mzE - mzS) * resolution), dtype=np.float32)
    interp = scipy.interpolate.griddata(scan['mz'].astype(np.float32), scan['intens'].astype(np.float32),
                                        grid, fill_value=0.0, method=method)
    return grid, interp


def convert_to_uniform_grid_no_interpolation(scan, resolution=1000):
    mzS     = np.floor(scan['mz'][0])
    mzE     = np.ceil(scan['mz'][-1])
    mz_grid = np.linspace(mzS, mzE, int((mzE - mzS) * resolution), dtype=np.float32)
    idxs    = ((scan['mz'] - mzS) * resolution).astype(np.int32)
    intens_grid = np.zeros(mz_grid.shape, dtype=np.float32)
    intens_grid[idxs] = scan['intens']
    return mz_grid, intens_grid, idxs


def _expmodgauss(tau, sigma, window_size=101):
    frozen_dist = scipy.stats.exponnorm(K=tau, loc=0.0, scale=sigma)
    return frozen_dist.pdf(np.linspace(frozen_dist.ppf(0.001), frozen_dist.ppf(0.999), window_size))


def convolve_uniform_grid(signal, window='gaussian', sigma=0.01, resolution=1000, std=1.0, tau=1.0, mode='same', plot=False):
    convwin = {
        'gaussian':    lambda: scipy.signal.windows.gaussian(M=int(sigma * 6 * resolution), std=sigma * resolution),
        'expmodgauss': lambda: _expmodgauss(tau, sigma),
        'hann':        lambda: scipy.signal.windows.hann(M=int(sigma * 6 * resolution))
    }[window]()
    convwin  /= convwin.sum()
    convolved = np.convolve(signal, convwin, mode=mode)

    if plot:
        # %matplotlib inline
        fig, ax = plt.subplots(figsize=(16, 5))
        ax.plot(signal,    label='Signal')
        ax.plot(convolved, label='Convolved')
        ax.set_title('Gaussian Convolution');
        ax.legend()

    return convolved


def local_maxima(arr):
    return np.where(np.r_[True, arr[1:] > arr[:-1]] & np.r_[arr[:-1] > arr[1:], True])[0]


def local_maxima_uniform_grid(signal, mz_grid, intens_grid, plot=False):
    idxs = local_maxima(signal)

    if plot:
        # %matplotlib inline
        fig, ax = plt.subplots(figsize=(16, 5))
        ax.vlines(mz_grid[idxs], 0, intens_grid[idxs], 'b')
        ax.set_title('Local Max Peaks');

    return mz_grid[idxs], intens_grid[idxs], idxs


def build_isotope_window(charge, resolution=1000, win_num_isotopes=5, isotope_perc_width=0.01):
    base_mz_delta    = 1.0031
    isotope_delta    = base_mz_delta / charge
    isotope_width    = isotope_delta * isotope_perc_width
    win_size         = int(round((((win_num_isotopes - 1) * isotope_delta) + isotope_width) * resolution))
    isotope_window   = np.zeros(win_size)
    for i in range(win_num_isotopes):
        lbound = int(round((i * isotope_delta) * resolution))
        ubound = int(round((i * isotope_delta + isotope_width) * resolution))
        isotope_window[lbound:ubound].fill(1.0)

    isotope_window /= isotope_window.sum()
    return isotope_window


def apply_peak_isotope_score(local_max_idxs, local_max_intens, mz_grid, weight=5.0, plot=False):
    grid = np.zeros(mz_grid.shape)
    grid[local_max_idxs] = local_max_intens
    convolved_list = [np.convolve(grid, window, mode='same') for window in ISOTOPE_WINDOWS]
    convolved_max  = np.amax(np.vstack(convolved_list), axis=0)
    scored_intens  = local_max_intens + (convolved_max[local_max_idxs] * weight)

    if plot:
        fig, ax = plt.subplots(1, 1, figsize=(20, 6))
        l1  = ax.vlines(mz_grid[local_max_idxs], 0, scored_intens,    'r', label='Local Max + Iso. Score')
        l2  = ax.vlines(mz_grid[local_max_idxs], 0, local_max_intens, 'b', label='Local Max')
        leg = plt.legend()
        setup_toggable_legend_lines(leg, fig, [l1, l2])
        plt.subplots_adjust(left=0, right=0.999)
        plt.show()

    return scored_intens


# TODO: refactor signature and use "scan" as in prune_with_intensity 
def prune_with_gradient(local_max_mzs, local_max_intens, local_max_idxs, snr=1.0, kernel_size=11,
                        plot=False, verbose=False):
    if len(local_max_intens) < (kernel_size * 4):
        return local_max_mzs, local_max_intens, local_max_idxs

    sorted_intenss  = np.sort(local_max_intens)[::-1]
    gradient        = np.gradient(sorted_intenss)
    smooth_gradient = scipy.signal.medfilt(gradient, kernel_size=kernel_size)
    grad_threshold  = (0.1 * snr) ** 2
    cutoff_index    = np.where(smooth_gradient > -grad_threshold)[0][0]
    cutoff_intens   = sorted_intenss[cutoff_index]
    threshold_idxs  = np.where(local_max_intens > cutoff_intens)[0]
    pruned_idxs     = local_max_idxs[threshold_idxs]
    pruned_mzs      = local_max_mzs[threshold_idxs]
    pruned_intens   = local_max_intens[threshold_idxs]

    if verbose:
        display(f'Prune With Gradient | orig. # peaks: {len(local_max_intens)}; after: {len(pruned_intens)}')
    if plot:
        fig, ax = plt.subplots(1, 2, figsize=(16, 4))
        ax[0].plot(sorted_intenss,  label='Ranked Intensities')
        ax[1].plot(gradient,        label='Gradient')
        ax[1].plot(smooth_gradient, label='Smooth Gradient')
        plt.legend()
        plt.show()

    return pruned_mzs, pruned_intens, pruned_idxs


def convolve_with_downsampling(local_max_mzs, local_max_intens, mz_grid, window, subsample_ratio=100, plot=False):
    def _subsample_average(arr, n):
        end =  n * int(len(arr)/n)
        return np.mean(arr[:end].reshape(-1, n), 1)

    grid = np.copy(mz_grid)
    local_max_interpolated = scipy.interpolate.griddata(local_max_mzs, local_max_intens, mz_grid, fill_value=0.0, method='nearest')
    local_max_subsampled   = _subsample_average(local_max_interpolated, subsample_ratio)
    convolved              = np.convolve(local_max_subsampled, window / window.sum(), mode='same')
    convolved_upsampled    = scipy.interpolate.griddata(grid[::subsample_ratio], convolved, grid, method='linear')

    if plot:
        plt.plot(mz_grid, convolved_upsampled, 'y', label='Centroid Convolved')
        plt.vlines(lmax_mzs, 0, np.log(lmax_intens), 'b', label='Centroid')

    return convolved_upsampled


# TODO: refactor signature and use "scan" as in prune_with_intensity 
def prune_with_convolution(local_max_mzs, local_max_intens, local_max_idxs, mz_grid, intens_grid,
                           snr=1.0, window=scipy.signal.windows.cosine(M=201), plot=False, verbose=False):
    threshold_signal = convolve_with_downsampling(local_max_mzs, local_max_intens, mz_grid, window)
    threshold_idxs   = np.where(local_max_intens > threshold_signal[local_max_idxs] * snr)[0]
    pruned_idxs      = local_max_idxs[threshold_idxs]
    pruned_mzs       = local_max_mzs[threshold_idxs]
    pruned_intens    = local_max_intens[threshold_idxs]

    if verbose:
        display(f'Prune With Convolution | orig. # peaks: {len(local_max_intens)}; after: {len(pruned_intens)}')
    if plot:
        fig, ax = plt.subplots(1, 1, figsize=(16, 5))
        l0  = ax.plot(mz_grid, intens_grid,                      label='Profile')
        l1  = ax.plot(mz_grid, threshold_signal,                 label='Threshold')
        l2  = ax.vlines(local_max_mzs, 0, local_max_intens, 'y', label='Local Max. Peaks')
        l3  = ax.vlines(pruned_mzs,    0, pruned_intens,    'b', label='Local Max. Peaks Filtered')
        leg = plt.legend(loc='upper right')
        setup_toggable_legend_lines(leg, fig, [l0, l1, l2, l3])
        plt.subplots_adjust(left=0, right=0.999)
        plt.show()

    return pruned_mzs, pruned_intens, pruned_idxs


def prune_with_intensity(scan, snr=1.0, plot=False, verbose=False):
    mzs            = scan['peaks']['mz']
    intenss        = scan['gridconv'][scan['peaks']['indexes']] if 'gridconv' in scan else scan['peaks']['intens']
    limit          = np.percentile(intenss, snr * 10)
    pruned_idxs    = np.where(intenss > limit)[0]
    pruned_mzs     = mzs[pruned_idxs]
    pruned_intenss = scan['peaks']['intens'][pruned_idxs]

    if verbose:
        display(f'Prune With Intensity | orig. # peaks: {len(intenss)}; after: {len(pruned_intenss)}')
    if plot:
        fig, (ax, bx) = plt.subplots(1, 2, figsize=(13, 4))
        ax.plot(np.sort(intenss)[::-1],        'y', alpha=0.5, label='Peaks')
        ax.plot(np.sort(pruned_intenss)[::-1], 'b', alpha=0.7, label='Pruned')
        ax.legend()

        l0  = bx.vlines(scan['peaks']['mz'], 0, scan['peaks']['intens'], 'y', alpha=0.5, label='Peaks')
        l1  = bx.vlines(pruned_mzs,          0, pruned_intenss,          'b', alpha=0.5, label='Pruned')
        leg = plt.legend(loc='upper right')
        setup_toggable_legend_lines(leg, fig, [l0, l1])
        plt.subplots_adjust(left=0.05, right=0.95)
        plt.show()

    return pruned_mzs, pruned_intenss, pruned_idxs


ISOTOPE_WINDOWS = [build_isotope_window(charge, isotope_perc_width=0.01) for charge in range(1, 6)]
