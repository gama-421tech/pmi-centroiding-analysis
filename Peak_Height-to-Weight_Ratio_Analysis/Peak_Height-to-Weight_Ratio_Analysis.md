
<h3 style="margin-bottom: 1em;"><img src="https://www.proteinmetrics.com/wp-content/uploads/2017/08/logo.svg" width="320" style="display: inline; margin: 0 2em 0 0;" /> Peak Height-to-Weight Ratio Analysis</h3>


```python
import argparse
import re
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.signal as sig
from pyteomics import mzml
from scipy.optimize import curve_fit
```


```python
# uncomment when re-converting this to a python script

# # ------ main --------
# parser = argparse.ArgumentParser(description='compute chromatogram peak similarity')
# parser.add_argument('filename', help='mzML encoded file with the input spectra')
# args   = parser.parse_args()
# mzfile = mzml.read(args.filename, use_index=True)
```


```python
filename = '/home/gama/local/pmi/data/Mass Spectra/iPRG2012/prg.ms1.mzML'
mzfile   = mzml.read(filename, use_index=True)
```

### Define/tweak main experiment parameters:

1. The basic "peak picking" function (which should focus on high-intensity peaks, as one of the fundamental assumptions is that these "ease-to-spot" peaks will allow us to calculate the peak's "width", which we then extrapolate as the ideal kernel window for that _m/z_ range"


```python
# define single-scan 'find_peak_func' function; for now we use
# a simple function based on scipy's find_peaks with an additional
# thresholding step (select N highest peaks)
def find_peaks_func(intensities, mzs, min_height=1000, min_distance=2, max_peaks=20):
    peaks, props = sig.find_peaks(intensities, height=min_height, distance=min_distance)
    indexes      = props['peak_heights'].argsort()[::-1]
    peaks        = peaks[indexes][:max_peaks]
    # widths     = sig.peak_widths(intensities, peaks, rel_height=0.1)
    return peaks
```

2. The number of bins/buckets/partitions we split the m/z range


```python
nbins = 20
```

#### Define "find peak" functions, which returns panda DataFrames for further processing


```python
def find_peaks_spectra(spectra, scan_idx, find_peaks_func, window_size=100, peak_ratio=0.05):
    def _peak_ratio(peak_idx):
        threshold    = intenss[peak_idx] * peak_ratio
        low_neighbor = next(idx for idx in range(peak_idx, peak_idx - window_size, -1)
                            if intenss[idx] < threshold)
        up_neighbor  = next(idx for idx in range(peak_idx, peak_idx + window_size, 1)
                            if intenss[idx] < threshold)
        return (scan_idx, peak_idx, low_neighbor, up_neighbor,
                mzs[peak_idx], mzs[low_neighbor], mzs[up_neighbor],
                intenss[peak_idx], intenss[low_neighbor], intenss[up_neighbor],)
    
    scan      = spectra[scan_idx]
    mzs       = scan['m/z array']
    intenss   = scan['intensity array']
    peak_idxs = find_peaks_func(intenss, mzs)

    # index   = np.array([mzs[i] for i in peak_idxs], dtype=np.float32)
    columns = ['Scan Index', 'Peak Index', 'Lower Bound Index', 'Upper Bound Index', 'Peak M/Z', 'Lower Bound M/Z', 'Upper Bound M/Z', 'Peak Intensity', 'Lower Bound Intensity', 'Upper Bound Intensity']
    dtypes  = [np.int32,     np.int32,     np.int32,            np.int32,            np.float32, np.float32,        np.float32,        np.float32,       np.float32,              np.float32             ]
    ldata   = list(zip(*[_peak_ratio(peak_idx) for peak_idx in peak_idxs]))
    data    = {
        column: np.array(list, dtype=dtype)
        for column, list, dtype in zip(columns, ldata, dtypes)
    }
    return pd.DataFrame(data=data)


def find_peaks_index_range(spectra, find_peaks_func, _range, window_size=100, peak_ratio=0.05):
    df = None
    for scan_idx in (_range or range(0, len(spectra))):
        scan_df = find_peaks_spectra(mzfile, scan_idx, find_peaks_func, window_size, peak_ratio)
        df = scan_df if df is None else df.append(scan_df)
    df.reset_index(inplace=True)
    return df


def calculate_peak_ratios(df):
    df['Height/Width Ratio 1'] = df['Peak Intensity'] / \
        (2 * np.maximum(df['Peak M/Z'] - df['Lower Bound M/Z'], df['Upper Bound M/Z'] - df['Peak M/Z']))

    df['Height/Width Ratio 2'] = df['Peak Intensity'] / \
        (df['Upper Bound M/Z'] - df['Lower Bound M/Z'])

    df['Skew Ratio'] = np.abs((df['Peak M/Z'] - df['Lower Bound M/Z']) - (df['Upper Bound M/Z'] - df['Peak M/Z'])) / \
        df['Peak Intensity']

    return df


def partition_in_mz_bins(df, nbins = 25):
    bins = np.linspace(df['Peak M/Z'].min(), df['Peak M/Z'].max(), nbins)
    df['Peak M/Z Bucket'] = pd.cut(df['Peak M/Z'], bins=bins)
    return df
```

#### Build a DataFrame from the dataset using functions above


```python
# build pandas DataFrame with peaks found withing a given range of scans
df = find_peaks_index_range(mzfile, find_peaks_func, range(0, len(mzfile), 5))

# add "height-to-weight ratio" columns
df = calculate_peak_ratios(df)

# bucketize the data into N bins over the M/Z axis
df = partition_in_mz_bins(df, nbins=nbins)

display(df.describe())
display(df.sample(8))
```


<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>index</th>
      <th>Scan Index</th>
      <th>Peak Index</th>
      <th>Lower Bound Index</th>
      <th>Upper Bound Index</th>
      <th>Peak M/Z</th>
      <th>Lower Bound M/Z</th>
      <th>Upper Bound M/Z</th>
      <th>Peak Intensity</th>
      <th>Lower Bound Intensity</th>
      <th>Upper Bound Intensity</th>
      <th>Height/Width Ratio 1</th>
      <th>Height/Width Ratio 2</th>
      <th>Skew Ratio</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>9526.000000</td>
      <td>9526.000000</td>
      <td>9526.000000</td>
      <td>9526.000000</td>
      <td>9526.000000</td>
      <td>9526.000000</td>
      <td>9526.000000</td>
      <td>9526.000000</td>
      <td>9526.000000</td>
      <td>9526.000000</td>
      <td>9526.000000</td>
      <td>9.526000e+03</td>
      <td>9.526000e+03</td>
      <td>9526.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>8.665547</td>
      <td>2235.066660</td>
      <td>25981.784380</td>
      <td>25974.589754</td>
      <td>25992.542305</td>
      <td>591.138123</td>
      <td>591.113403</td>
      <td>591.174988</td>
      <td>6571.355469</td>
      <td>227.359955</td>
      <td>239.847366</td>
      <td>1.016129e+05</td>
      <td>1.226461e+05</td>
      <td>0.000005</td>
    </tr>
    <tr>
      <th>std</th>
      <td>5.775852</td>
      <td>797.163158</td>
      <td>19505.369499</td>
      <td>19505.113322</td>
      <td>19506.014812</td>
      <td>151.340485</td>
      <td>151.336014</td>
      <td>151.347473</td>
      <td>21460.843750</td>
      <td>846.542358</td>
      <td>805.142212</td>
      <td>4.119373e+05</td>
      <td>4.796908e+05</td>
      <td>0.000006</td>
    </tr>
    <tr>
      <th>min</th>
      <td>0.000000</td>
      <td>260.000000</td>
      <td>55.000000</td>
      <td>46.000000</td>
      <td>65.000000</td>
      <td>400.231995</td>
      <td>400.212219</td>
      <td>400.257416</td>
      <td>1000.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>5.353985e+03</td>
      <td>7.401633e+03</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>4.000000</td>
      <td>1595.000000</td>
      <td>10884.250000</td>
      <td>10876.250000</td>
      <td>10894.500000</td>
      <td>474.233917</td>
      <td>474.212402</td>
      <td>474.276978</td>
      <td>1866.250000</td>
      <td>53.000000</td>
      <td>58.000000</td>
      <td>2.420067e+04</td>
      <td>2.961263e+04</td>
      <td>0.000002</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>8.000000</td>
      <td>2192.500000</td>
      <td>21856.000000</td>
      <td>21846.000000</td>
      <td>21865.500000</td>
      <td>555.276245</td>
      <td>555.254578</td>
      <td>555.311157</td>
      <td>3084.500000</td>
      <td>96.000000</td>
      <td>105.000000</td>
      <td>4.256038e+04</td>
      <td>5.145861e+04</td>
      <td>0.000004</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>14.000000</td>
      <td>2790.000000</td>
      <td>36689.500000</td>
      <td>36683.500000</td>
      <td>36703.000000</td>
      <td>663.769211</td>
      <td>663.747375</td>
      <td>663.810150</td>
      <td>5870.000000</td>
      <td>201.000000</td>
      <td>213.000000</td>
      <td>8.548140e+04</td>
      <td>1.044213e+05</td>
      <td>0.000007</td>
    </tr>
    <tr>
      <th>max</th>
      <td>19.000000</td>
      <td>4550.000000</td>
      <td>108894.000000</td>
      <td>108885.000000</td>
      <td>108909.000000</td>
      <td>1208.630371</td>
      <td>1208.600952</td>
      <td>1208.684448</td>
      <td>819563.000000</td>
      <td>33146.000000</td>
      <td>34511.000000</td>
      <td>1.668040e+07</td>
      <td>1.905993e+07</td>
      <td>0.000065</td>
    </tr>
  </tbody>
</table>
</div>



<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>index</th>
      <th>Scan Index</th>
      <th>Peak Index</th>
      <th>Lower Bound Index</th>
      <th>Upper Bound Index</th>
      <th>Peak M/Z</th>
      <th>Lower Bound M/Z</th>
      <th>Upper Bound M/Z</th>
      <th>Peak Intensity</th>
      <th>Lower Bound Intensity</th>
      <th>Upper Bound Intensity</th>
      <th>Height/Width Ratio 1</th>
      <th>Height/Width Ratio 2</th>
      <th>Skew Ratio</th>
      <th>Peak M/Z Bucket</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>9209</th>
      <td>12</td>
      <td>3830</td>
      <td>11018</td>
      <td>11014</td>
      <td>11029</td>
      <td>643.986633</td>
      <td>643.972351</td>
      <td>644.026062</td>
      <td>1287.0</td>
      <td>63.0</td>
      <td>63.0</td>
      <td>16320.594727</td>
      <td>23961.599609</td>
      <td>1.953884e-05</td>
      <td>(612.968, 655.516]</td>
    </tr>
    <tr>
      <th>3975</th>
      <td>12</td>
      <td>1995</td>
      <td>19102</td>
      <td>19095</td>
      <td>19112</td>
      <td>520.332764</td>
      <td>520.310242</td>
      <td>520.364990</td>
      <td>5220.0</td>
      <td>237.0</td>
      <td>129.0</td>
      <td>80989.093750</td>
      <td>95345.015625</td>
      <td>1.859117e-06</td>
      <td>(485.327, 527.874]</td>
    </tr>
    <tr>
      <th>3255</th>
      <td>12</td>
      <td>1815</td>
      <td>4134</td>
      <td>4126</td>
      <td>4144</td>
      <td>434.230286</td>
      <td>434.206726</td>
      <td>434.259705</td>
      <td>3881.0</td>
      <td>0.0</td>
      <td>84.0</td>
      <td>65960.898438</td>
      <td>73256.109375</td>
      <td>1.509759e-06</td>
      <td>(400.232, 442.779]</td>
    </tr>
    <tr>
      <th>1129</th>
      <td>18</td>
      <td>1265</td>
      <td>6218</td>
      <td>6206</td>
      <td>6230</td>
      <td>421.226501</td>
      <td>421.191742</td>
      <td>421.261292</td>
      <td>2274.0</td>
      <td>83.0</td>
      <td>83.0</td>
      <td>32681.767578</td>
      <td>32696.109375</td>
      <td>1.342022e-08</td>
      <td>(400.232, 442.779]</td>
    </tr>
    <tr>
      <th>902</th>
      <td>16</td>
      <td>1205</td>
      <td>60522</td>
      <td>60511</td>
      <td>60531</td>
      <td>965.366821</td>
      <td>965.318542</td>
      <td>965.406311</td>
      <td>1596.0</td>
      <td>55.0</td>
      <td>72.0</td>
      <td>16528.990234</td>
      <td>18184.189453</td>
      <td>5.506931e-06</td>
      <td>(953.347, 995.894]</td>
    </tr>
    <tr>
      <th>4464</th>
      <td>1</td>
      <td>2120</td>
      <td>29859</td>
      <td>29852</td>
      <td>29869</td>
      <td>569.313782</td>
      <td>569.290222</td>
      <td>569.347473</td>
      <td>28636.0</td>
      <td>1105.0</td>
      <td>913.0</td>
      <td>424974.843750</td>
      <td>500183.593750</td>
      <td>3.538146e-07</td>
      <td>(527.874, 570.421]</td>
    </tr>
    <tr>
      <th>9407</th>
      <td>5</td>
      <td>4195</td>
      <td>36312</td>
      <td>36304</td>
      <td>36323</td>
      <td>647.359375</td>
      <td>647.330627</td>
      <td>647.398865</td>
      <td>1046.0</td>
      <td>51.0</td>
      <td>51.0</td>
      <td>13243.944336</td>
      <td>15328.858398</td>
      <td>1.026978e-05</td>
      <td>(612.968, 655.516]</td>
    </tr>
    <tr>
      <th>8744</th>
      <td>2</td>
      <td>3375</td>
      <td>54561</td>
      <td>54554</td>
      <td>54573</td>
      <td>871.485229</td>
      <td>871.456055</td>
      <td>871.535217</td>
      <td>6644.0</td>
      <td>225.0</td>
      <td>231.0</td>
      <td>66456.226562</td>
      <td>83928.523438</td>
      <td>3.132599e-06</td>
      <td>(868.252, 910.799]</td>
    </tr>
  </tbody>
</table>
</div>


#### Calculate number of peaks per M/Z Bucket and plot histogram


```python
columns   = list(filter(lambda x: re.search(r'Ratio|M\/Z|Intensity', x), df.columns))
gdf       = df.groupby(by='Peak M/Z Bucket')[columns]
count_gdf = gdf.count()
count_gdf.plot.bar(y='Peak M/Z', title='# of peaks per m/z bucket', figsize=(13, 5,));
```


![png](output_14_0.png)


#### Create DataFrame with _averages


```python
mean_gdf = gdf.mean()
mean_gdf['M/Z Bin'] = np.array([interval.mid for interval in mean_gdf.index])
display(mean_gdf.sample(4))
mean_gdf.plot(x='M/Z Bin', y='Height/Width Ratio 1', figsize=(13, 5));
```


<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Peak M/Z</th>
      <th>Lower Bound M/Z</th>
      <th>Upper Bound M/Z</th>
      <th>Peak Intensity</th>
      <th>Lower Bound Intensity</th>
      <th>Upper Bound Intensity</th>
      <th>Height/Width Ratio 1</th>
      <th>Height/Width Ratio 2</th>
      <th>Skew Ratio</th>
      <th>M/Z Bin</th>
    </tr>
    <tr>
      <th>Peak M/Z Bucket</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>(570.421, 612.968]</th>
      <td>590.081360</td>
      <td>590.056519</td>
      <td>590.118225</td>
      <td>5504.932617</td>
      <td>183.279541</td>
      <td>199.329498</td>
      <td>76628.539062</td>
      <td>92939.546875</td>
      <td>0.000005</td>
      <td>591.6945</td>
    </tr>
    <tr>
      <th>(612.968, 655.516]</th>
      <td>633.207642</td>
      <td>633.181702</td>
      <td>633.246582</td>
      <td>5936.833008</td>
      <td>205.233826</td>
      <td>222.053345</td>
      <td>78092.562500</td>
      <td>95512.554688</td>
      <td>0.000005</td>
      <td>634.2420</td>
    </tr>
    <tr>
      <th>(698.063, 740.61]</th>
      <td>717.700195</td>
      <td>717.673035</td>
      <td>717.743591</td>
      <td>3960.281006</td>
      <td>129.943146</td>
      <td>141.779266</td>
      <td>45881.269531</td>
      <td>57518.203125</td>
      <td>0.000008</td>
      <td>719.3365</td>
    </tr>
    <tr>
      <th>(485.327, 527.874]</th>
      <td>505.747498</td>
      <td>505.725739</td>
      <td>505.780090</td>
      <td>6679.154297</td>
      <td>227.471909</td>
      <td>248.658905</td>
      <td>105729.882812</td>
      <td>128341.398438</td>
      <td>0.000004</td>
      <td>506.6005</td>
    </tr>
  </tbody>
</table>
</div>



![png](output_16_1.png)


#### Do _curving fitting_ on the height-to-width ratio points using a neg. exponential


```python
def regress_func(x, a, b, c):
    return a * np.exp(-b * x) + c

xdata = mean_gdf['M/Z Bin'].values
ydata = mean_gdf['Height/Width Ratio 1'].values
popt, pcov = curve_fit(regress_func, xdata, ydata, p0=[250000, 0.005, 0.5])
# display(popt, pcov)

fig = plt.figure(figsize=(13, 6,))
ax  = fig.add_subplot(111)
ax.plot(xdata, ydata, 'b-', label='data')
ax.plot(xdata, regress_func(xdata, *popt), 'y-',
        label='fit: a=%5.6f, b=%5.6f, c=%5.6f' % tuple(popt));
ax.legend();
```


![png](output_18_0.png)


#### Define functions that returns a gaussian kernel "width" for a given `mz` & `intensity`


```python
def height_to_width_ratio(mz):
    return regress_func(mz, *popt)

def conv_window_width(mz, intens):
    return intens / hw_ratio(mz)

def gaussian(x, amplitude, mean= 0.0, stddev=0.05):
    return amplitude * np.exp(-((x - mean) / 4 / stddev)**2)
```

#### Plot and overlay of the the gaussians of multiple m/z bins


```python
def plot_gaussian(title, ax, stddev_func, xlim=(-5, 5), ylim=(0, 1.02), yoffset=0.0):
    mean  = 0.0
    xdata = np.linspace(xlim[0] * 1.5, xlim[1] * 1.5, 1000)
    
    for mz in range(400, 1200, 100):
        width  = height_to_width_ratio(mz)
        ydata  = gaussian(x, 1.0 + yoffset, mean, stddev_func(width)) - yoffset
        label  = '{:4d} mz; {:.3f}sd'.format(mz, stddev_func(width))
        ax.plot(xdata, ydata, label=label)
        ax.legend()

    ax.set_xlim(*xlim)
    ax.set_ylim(*ylim)
    ax.set_title(title)

linear_stddev  = lambda width: 1200 / width
squared_stddev = lambda width: 12   / (width ** (1/2))

fig = plt.figure(figsize=(20, 14,))
plot_gaussian('Linear Gaussian Curves',          fig.add_subplot(221), linear_stddev)
plot_gaussian('Linear Clipped Gaussian Curves',  fig.add_subplot(222), linear_stddev, yoffset=0.2)
plot_gaussian('Squared Gaussian Curves',         fig.add_subplot(223), squared_stddev)
plot_gaussian('Squared Clipped Gaussian Curves', fig.add_subplot(224), squared_stddev, yoffset=0.2)
```


![png](output_22_0.png)


#### Extra: peak plotting functions, to visualize a m/z bin's "peak shape"


```python
def plot_peak(ax, spectra, sample_data, normalize=True, max_intens=None, span=None):
    def _calc_padding(span, values):
        pad = (span - (values[-1] - values[0])) / 2
        return (values[0]  - pad), (values[-1] + pad)
    
    index    = int(sample_data['Scan Index'])
    peak_idx = int(sample_data['Peak Index'])
    scan     = spectra[index]
    low_idx  = sample_data['Lower Bound Index']
    up_idx   = sample_data['Upper Bound Index']
    mz       = scan['m/z array'][low_idx:up_idx]
    intens   = scan['intensity array'][low_idx:up_idx]
    label    = 'm/z: {:.3f}'.format(scan['m/z array'][peak_idx])
    if normalize:
        mz     = (mz     - mz.min())     / mz.ptp()
        intens = (intens - intens.min()) / intens.ptp()
        label  = 'norm ' + label
        
    ax.plot(mz, intens, label=label)
    ax.legend()
    if not normalize:
        ax.set_ylim(0, max_intens)
        ax.set_xlim(_calc_padding(span, mz))

    return ax
        

def plot_peak_samples(spectra, df, bin_idx, nsamples=8):
    bin_val    = df['Peak M/Z Bucket'].cat.categories[bin_idx]
    df_bin     = df[df['Peak M/Z Bucket'] == bin_val]
    nrows      = nsamples
    fig        = plt.figure(figsize=(6, nrows * 6))
    sample     = df_bin.sample(nsamples)
    span       = np.amax(sample['Upper Bound M/Z'] - sample['Lower Bound M/Z'])
    max_intens = np.amax(sample['Peak Intensity']) * 1.05
    for (i, (_, sample_data,),) in enumerate(sample.iterrows()):
        ax = fig.add_subplot(nrows, 2, 2 * i + 1)
        plot_peak(ax, spectra, sample_data, False, max_intens, span)

        ax = fig.add_subplot(nrows, 2, 2 * i + 2)
        plot_peak(ax, spectra, sample_data)


def plot_peak_overlay(spectra, df, bin_idx, nsamples=8):
    bin_val = df['Peak M/Z Bucket'].cat.categories[bin_idx]
    df_bin  = df[df['Peak M/Z Bucket'] == bin_val]
    sample  = df_bin.sample(nsamples)
    fig     = plt.figure(figsize=(6, 6))
    ax      = fig.add_subplot(111)
    for _, sample_data in sample.iterrows():
        plot_peak(ax, spectra, sample_data)
```

#### Plot sample peaks of a few, distance m/z bins


```python
plot_peak_overlay(mzfile, df, bin_idx=1,  nsamples=3)
plot_peak_overlay(mzfile, df, bin_idx=7,  nsamples=3)
plot_peak_overlay(mzfile, df, bin_idx=14, nsamples=3)
```


![png](output_26_0.png)



![png](output_26_1.png)



![png](output_26_2.png)


#### Plot individual plots of several peaks of a given m/z bin (2nd)


```python
plot_peak_samples(mzfile, df, bin_idx=2, nsamples=8)
```


![png](output_28_0.png)

