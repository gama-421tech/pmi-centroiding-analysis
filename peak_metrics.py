# %load peak_metrics.py
import numpy as np
import pandas as pd
import scipy

EXPECTED_DIFF1 = 1.00310
EXPECTED_DIFF1_INTERVALS = (
    EXPECTED_DIFF1,
    EXPECTED_DIFF1 * (1 - 0.008),
    EXPECTED_DIFF1 * (1 - 0.003),
    EXPECTED_DIFF1 * (1 + 0.003),
    EXPECTED_DIFF1 * (1 + 0.008),
)
EXPECTED_MZ_INTERVALS = [
    tuple([ val / i for val in EXPECTED_DIFF1_INTERVALS])
    for i in range(1, 13)
];
EXPECTED_MZ_DIFFS = np.array([i[0] for i in EXPECTED_MZ_INTERVALS])


def build_deltas_dataframe(scans, NDIFFS=4, MAXDIFF=1.2):
    def _mzdiff(mz, i):
        diff = (mz[i:] - mz[:-i])[:mz.size - NDIFFS]
        diff[diff > MAXDIFF] = np.NaN
        return diff

    def _inv_rank(intens, i, exp=1.0):
        inv_rank = 1 / ((np.argsort(np.argsort(intens)[::-1]) + 1) ** exp)
        # return ((inv_rank[i:] + inv_rank[:-i]) / 2)[:intens.size - NDIFFS]
        return np.fmin(inv_rank[i:], inv_rank[:-i])[:intens.size - NDIFFS]

    def _diff_rowdata(scan):
        ids        = np.full((scan['mz'].size - NDIFFS,), scan['id'], dtype=np.int32)
        mzs        = scan['mz'][:-NDIFFS]
        intens     = scan['intens'][:-NDIFFS]
        inv_ranks  = 1 / (np.argsort(scan['intens'])[::-1] + 1)
        inv_ranks2 = 1 / ((np.argsort(scan['intens'])[::-1] + 1) ** 2)
        return np.stack(
            [ids, np.arange(mzs.size), mzs, intens] +
            [_inv_rank(scan['intens'], i, 1) for i in range(1, NDIFFS + 1)] +
            [_inv_rank(scan['intens'], i, 2) for i in range(1, NDIFFS + 1)] +
            [_mzdiff(scan['mz'], i) for i in range(1, NDIFFS + 1)],
            axis=1
        )

    data = np.concatenate([
        _diff_rowdata(scan)
        for scan in scans
        if scan['mz'].size >= NDIFFS
    ], axis=0)
    columns = np.array(['scan id', 'peak id', 'mz', 'intens'] +
                       ['1/rank{:02d}'.format(i)  for i in range(1, NDIFFS + 1)] +
                       ['1/rank²{:02d}'.format(i) for i in range(1, NDIFFS + 1)] +
                       ['diff{:02d}'.format(i)    for i in range(1, NDIFFS + 1)])

    df = pd.DataFrame(columns=columns, data=data)
    df['scan id'] = df['scan id'].astype('int32')
    df['peak id'] = df['peak id'].astype('int32')
    return df


def build_agg_deltas_dataframe(df):
    diff_cols       = [col for col in df.columns if col.startswith('diff')]
    inv_rank_cols   = [col for col in df.columns if col.startswith('1/rank') and '²' not in col]
    inv_rank2_cols  = [col for col in df.columns if col.startswith('1/rank²')]
    return pd.DataFrame({
        'diff':    np.concatenate([df[col] for col in diff_cols]),
        '1/rank':  np.concatenate([df[col] for col in inv_rank_cols]),
        '1/rank²': np.concatenate([df[col] for col in inv_rank2_cols])
    })


def plot_delta_weighted_histograms(df, delta, weights=['1/rank'], title='', suptitle=None):
    nbins            = 500
    full_bar_width   = 0.005   # df[col].max() / nbins
    detail_bar_width = 0.002
    col              = f'diff{delta}'
    fdf              = df[~df[col].isnull()]
    weights_map      = {'Original': None,
                        '1/rank':   fdf[f'1/rank{delta}'],
                        '1/rank²':  fdf[f'1/rank²{delta}']}

    fig, axes = plt.subplots(len(weights), 4, figsize=(16, len(weights) * 4), sharex='col',
                             squeeze=False, gridspec_kw={'width_ratios': [3, 1, 1, 1]})
    fig.suptitle(suptitle)

    # --- original
    for row, weights_label in enumerate(weights):
        hist, edges = np.histogram(fdf[col], bins=nbins, weights=weights_map[weights_label])
        axes[row][0].bar(edges[:-1], hist, width=full_bar_width)
        axes[row][0].set_title(f'{title} | {weights_label}')

        axes[row][1].bar(edges[:-1], hist, width=detail_bar_width)
        axes[row][1].set_title(f'{title} | {weights_label} @ 1.0')
        axes[row][1].set_xlim(0.95, 1.05)

        axes[row][2].bar(edges[:-1], hist, width=detail_bar_width)
        axes[row][2].set_title(f'{title} | {weights_label} @ 0.5')
        axes[row][2].set_xlim(0.45, 0.55)

        axes[row][3].bar(edges[:-1], hist, width=detail_bar_width)
        axes[row][3].set_title(f'{title} | {weights_label} @ 0.33')
        axes[row][3].set_xlim(0.3033, 0.3633)

    fig.subplots_adjust(wspace=0.20, top=0.80)


def basic_delta_stats(df, col, in_range_percentage=0.5, num_deltas=4):
    def _points_in_range(ref_value):
        abs_range = (ref_value * in_range_percentage / 100.0)
        return df[col][abs(df[col] - ref_value) < abs_range]

    def _nearby_point_stats(expected_diff):
        points = _points_in_range(expected_diff)
        deltas = points - expected_diff
        return (expected_diff, points.count(), deltas.std(),
                scipy.stats.variation(deltas.values, nan_policy='omit'),)

    data    = [_nearby_point_stats(diff) for diff in EXPECTED_MZ_DIFFS[:num_deltas]]
    columns = ['Expected Δ', 'Count', 'StdDev', 'Variation']
    return pd.DataFrame(data=data, columns=columns)


def print_basic_delta_stats(df, col, title, in_range_percentage):
    statsdf = basic_delta_stats(df, col, in_range_percentage)
    display(HTML('<h4>{}</h4>'.format(title)))
    display(statsdf)
    display(HTML('<span># points within {}% of expected diffs: </span>: <b>{} ({:.2f}%)</b>'.format(
        in_range_percentage, statsdf['count'].sum(), 100.0 * statsdf['count'].sum() / series.size)))
    display(HTML('<hr/>'))


def mbern_stats(df, col, num_deltas=4):
    deltas = df[col].values
    deltas = deltas[~np.isnan(deltas)]

    stats = []
    for exp_delta, lower_out, lower_in, upper_in, upper_out in EXPECTED_MZ_INTERVALS[:num_deltas]:
        inrange     = deltas[(deltas >= lower_in)  & (deltas <= upper_in)]
        loutrange   = deltas[(deltas >= lower_out) & (deltas <  lower_in)]
        uoutrange   = deltas[(deltas >  upper_in)  & (deltas <= upper_out)]
        outrange    = np.concatenate((loutrange, uoutrange,))
        inout_ratio = inrange.size / (outrange.size / 2) if outrange.size else 0
        stats.append([col, exp_delta, inrange.size, outrange.size, inout_ratio])
    columns = ['Δ', 'Expected Δ', '# In', '# Out', 'In/(Out/2)']
    return pd.DataFrame(data=stats, columns=columns)


def print_mbern_stats(df, col):
    display(Markdown('#### All Δs'))
    display(mbern_stats(df, col))
    display(HTML('<br/>'))


def peak_deltas_report(dataframe_map, sections=['basic-stats', 'mbern-metric', 'histograms']):
    if 'basic-stats' in sections:
        peak_deltas_basic_stats_report(dataframe_map)
    if 'mbern-metric' in sections:
        peak_deltas_mbern_metric_report(dataframe_map)
    if 'histograms' in sections:
        peak_deltas_histograms_report(dataframe_map, weights=['1/rank'])


def peak_deltas_basic_stats_report(dataframe_map):
    agg_stats_df = pd.DataFrame()
    for identifier, df in dataframe_map.items():
        stats_df = basic_delta_stats(df, 'diff', num_deltas=3)
        stats_df.insert(0, 'Identifier', [identifier] * len(stats_df))
        agg_stats_df = agg_stats_df.append(stats_df)

    for key, df in agg_stats_df.groupby('Expected Δ'):
        df.index = df['Identifier']
        display(Markdown(f'---\n##### Basic Stats | Expected Δ: {key}'))
        display(df.drop(columns=['Expected Δ', 'Identifier']))

    display(Markdown('---\n#### % of items near expected Δs'))
    filenames, perc_deltas = list(zip(*[
        (identifier, df['Count'].sum() / len(dataframe_map[identifier]) * 100.0,)
        for identifier, df in agg_stats_df.groupby('Identifier')
    ]))
    display(pd.DataFrame(columns=['% points near Δ'], data=perc_deltas, index=filenames))


def peak_deltas_mbern_metric_report(dataframe_map):
    agg_mbern_df = pd.DataFrame()
    for filename, df in dataframe_map.items():
        # mbern_stats(df, 'diff01')
        # mbern_stats(df, 'diff02')
        mbern_df = mbern_stats(df, 'diff', num_deltas=3)
        mbern_df['Filename'] = filename
        agg_mbern_df = agg_mbern_df.append(mbern_df)

    for key, df in agg_mbern_df.groupby('Expected Δ'):
        display(Markdown(f'---\n##### M/Z Accuracy (mbern\'s metric) | Expected Δ: {key}'))
        df.index = df['Filename']
        display(df.drop(columns=['Expected Δ', 'Δ', 'Filename']))


def peak_deltas_histograms_report(dataframe_map, weights=['Original', '1/rank', '1/rank²']):
    for filename, df in dataframe_map.items():
        # plot_delta_weighted_histograms(df, '01', 'Δ+1')
        # plot_delta_weighted_histograms(df, '02', 'Δ+2')
        plot_delta_weighted_histograms(df, '', weights=weights, title=f'All Δs', suptitle=filename)
