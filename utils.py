import numpy as np
import scipy


# https://stackoverflow.com/questions/29156532/python-baseline-correction-library
def baseline_als(y, lam=100, p=0.01, niter=10):
    L = len(y)
    diag = np.ones(L - 2)
    D = scipy.sparse.spdiags([diag, -2*diag, diag], [0, -1, -2], L, L-2).tocsc()
    w = np.ones(L)
    for i in range(niter):
        W = scipy.sparse.spdiags(w, 0, L, L)
        Z = W + lam * D.dot(D.transpose())
        z = scipy.sparse.linalg.spsolve(Z, w*y)
        w = p * (y > z) + (1 - p) * (y < z)
    return z


def set_xlim_and_correct_ylim(ax, lbound, ubound, x, y):
    ax.set_xlim(lbound, ubound)
    lims = ax.get_xlim()
    idx  = np.where((x > lims[0]) & (x < lims[1]))[0]
    ax.set_ylim(y[idx].min(), y[idx].max() * 1.05)


def setup_toggable_legend_lines(legend, fig, ax_lines):
    lined = dict()
    for legline, origline in zip(legend.get_lines(), ax_lines):
        origline = origline if hasattr(origline, '__iter__') else [origline]
        legline.set_picker(5)  # 5 pts tolerance
        lined[legline] = origline

    def _onpick(event):
        legline  = event.artist
        origline = lined[legline]
        visible  = not origline[0].get_visible()
        [l.set_visible(visible) for l in origline]
        legline.set_alpha(1.0 if visible else 0.2)
        # fig.canvas.draw()
        display('onpick')

    fig.canvas.mpl_connect('pick_event', _onpick)
    return lined


def plot_lines_and_peaks(*series):
    fig, ax = plt.subplots(1, 1, figsize=(15, 6))
    lines   = [ax.plot(data['x'],      data['y'], label=data['label']) for data in series if data['type'] == 'line']
    peaks   = [ax.vlines(data['x'], 0, data['y'], label=data['label']) for data in series if data['type'] == 'peaks']
    legend  = plt.legend(loc='upper right')
    setup_toggable_legend_lines(legend, fig, lines + peaks)
    plt.subplots_adjust(left=0, right=0.99)
    plt.show()


def toggle_line(ax, idx):
    ax.lines[idx].set_visible(not ax.lines[idx].get_visible())


def toggle_collection(ax, idx):
    ax.collections[idx].set_visible(not ax.collections[idx].get_visible())
