@echo on

:: set "BYSPEC2_OFFSET=2000"
:: set "BYSPEC2_LIMIT=1000"

:: ----- CDifficile -----
set "MASS_SPECTRA_FILES=E:\Mass Spectra\CDifficile delta\Converted\cdifficile.centroid-00050-00125-1000.byspec2;E:\Mass Spectra\CDifficile delta\Converted\cdifficile.centroid-001-001-0-1000.byspec2;E:\Mass Spectra\CDifficile delta\Converted\cdifficile.centroid-01-01-0-1000.byspec2;E:\Mass Spectra\CDifficile delta\Converted\cdifficile.centroid-vendor.byspec2"
jupyter nbconvert --ExecutePreprocessor.timeout=600 --to html --no-input --output="Centroid Peak Deltas - CDifficile.html" --execute "Peak Deltas.ipynb"


:: ----- iPGR2012 -----
set "MASS_SPECTRA_FILES=E:\Mass Spectra\iPRG2012\Converted\iPRG2012.centroid-00025-00050-2-1000.byspec2;E:\Mass Spectra\iPRG2012\Converted\iPRG2012.centroid-001-001-0-1000.byspec2;E:\Mass Spectra\iPRG2012\Converted\iPRG2012.centroid-01-01-0-1000.byspec2;E:\Mass Spectra\iPRG2012\Converted\iPRG2012.centroid-vendor.byspec2"
jupyter nbconvert --ExecutePreprocessor.timeout=600 --to html --no-input --output="Centroid Peak Deltas - iPRG2012.html" --execute "Peak Deltas.ipynb"
 
 
:: ----- MAM\1448 -----
set "MASS_SPECTRA_FILES=E:\Mass Spectra\MAM\1448\Converted\1448.centroid-000033-000750-1000.byspec2;E:\Mass Spectra\MAM\1448\Converted\1448.centroid-001-001-0-1000.byspec2;E:\Mass Spectra\MAM\1448\Converted\1448.centroid-01-01-0-1000.byspec2;E:\Mass Spectra\MAM\1448\Converted\1448.centroid-vendor.byspec2"
jupyter nbconvert --ExecutePreprocessor.timeout=600 --to html --no-input --output="Centroid Peak Deltas - MAM1448.html" --execute "Peak Deltas.ipynb"


:: ----- MAM\4332 -----
set "MASS_SPECTRA_FILES=E:\Mass Spectra\MAM\4332\Converted\4332.centroid-000040-000666-1000.byspec2;E:\Mass Spectra\MAM\4332\Converted\4332.centroid-001-001-0-1000.byspec2;E:\Mass Spectra\MAM\4332\Converted\4332.centroid-01-01-0-1000.byspec2;E:\Mass Spectra\MAM\4332\Converted\4332.centroid-vendor.byspec2"
jupyter nbconvert --ExecutePreprocessor.timeout=600 --to html --no-input --output="Centroid Peak Deltas - MAM4332.html" --execute "Peak Deltas.ipynb"


:: ----- MAM\6544 -----
set "MASS_SPECTRA_FILES=E:\Mass Spectra\MAM\6544\Converted\6544.centroid-0003-0009-1000.byspec2;E:\Mass Spectra\MAM\6544\Converted\6544.centroid-001-001-0-1000.byspec2;E:\Mass Spectra\MAM\6544\Converted\6544.centroid-01-01-0-1000.byspec2;E:\Mass Spectra\MAM\6544\Converted\6544.centroid-vendor.byspec2"
jupyter nbconvert --ExecutePreprocessor.timeout=600 --to html --no-input --output="Centroid Peak Deltas - MAM6544.html" --execute "Peak Deltas.ipynb"


:: ----- MAM\8292 -----
set "MASS_SPECTRA_FILES=E:\Mass Spectra\MAM\8292\Converted\8292.centroid-0004-0015.byspec2;E:\Mass Spectra\MAM\8292\Converted\8292.centroid-001-001-0-1000.byspec2;E:\Mass Spectra\MAM\8292\Converted\8292.centroid-01-01-0-1000.byspec2;E:\Mass Spectra\MAM\8292\Converted\8292.centroid-vendor.byspec2"
jupyter nbconvert --ExecutePreprocessor.timeout=600 --to html --no-input --output="Centroid Peak Deltas - MAM8292.html" --execute "Peak Deltas.ipynb"
