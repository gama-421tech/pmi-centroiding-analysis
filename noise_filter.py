import numpy as np
import scipy
from uniform_grid import convert_to_uniform_grid
from uniform_grid import convert_to_uniform_grid_no_interpolation
from uniform_grid import convolve_uniform_grid
from uniform_grid import local_maxima_uniform_grid
from uniform_grid import prune_with_intensity
from uniform_grid import prune_with_convolution
from uniform_grid import prune_with_gradient
from utils        import plot_lines_and_peaks

def fwhm(X, Y, max_idx, frac=2.0, window_size=100):
    def interp(xa, ya, xb, yb, y, hmax, t='type'):
        if yb <= ya:
            raise RuntimeError(f'yb not greater than ya (ya={ya}, yb={yb}, hmax={y}, t={t})')
        return ((xb * (y - ya) + xa * (yb - y)) / (yb - ya))

    ymax       = Y[max_idx]
    half_max   = Y[max_idx] / 2.0
    left_idx   = max_idx - 1 - np.argmax(Y[max(0, max_idx - window_size) : max_idx][::-1] < half_max)
    right_idx  = max_idx     + np.argmax(Y[max_idx : max_idx + window_size] < half_max)
    left_val   = interp(X[left_idx],  Y[left_idx],  X[left_idx + 1],  Y[left_idx + 1],  half_max, 'left')
    right_val  = interp(X[right_idx], Y[right_idx], X[right_idx - 1], Y[right_idx - 1], half_max, 'right')
    full_width = right_val - left_val
    sigma      = full_width / 2.355   # gaussian; see wikipedia/Full_width_at_half_maximum
    return full_width, half_max, sigma, left_idx, right_idx, left_val, right_val


def estimate_sigma(scans):
    all_intenss = np.concatenate([scan['intens'] for scan in scans])
    perc98      = np.percentile(all_intenss, 98)
    sigmas      = []
    for scan in scans:
        scan_peaks, props = scipy.signal.find_peaks(scan['intens'], height=perc98, distance=100)
        scan_sigmas = [fwhm(scan['mz'], scan['intens'], peak)[2] for peak in scan_peaks]
        sigmas.extend(scan_sigmas)
    return np.median(sigmas)


def pick_peaks(scan, centroid=False, window='gaussian', sigma=0.07281, tau=0.5):
    if centroid:
        mz_grid, intens_grid, lmax_idxs = convert_to_uniform_grid_no_interpolation(scan)
        lmax_mzs, lmax_intens = mz_grid[lmax_idxs], intens_grid[lmax_idxs]
    else:
        mz_grid, intens_grid = convert_to_uniform_grid(scan)
        scan['gridconv']     = convolve_uniform_grid(intens_grid, window=window, sigma=sigma, tau=tau)
        lmax_mzs, lmax_intens, lmax_idxs = local_maxima_uniform_grid(scan['gridconv'], mz_grid, intens_grid)

    scan['grid']  = {'mz': mz_grid,  'intens': intens_grid}
    scan['peaks'] = {'mz': lmax_mzs, 'intens': lmax_intens, 'indexes': lmax_idxs}
    return scan


def filter_noise(scan, snr=1.0, algos=['revintens', 'conv', 'grad', 'isoconv', 'isograd'], plot=False):
    output      = dict(scan)
    mz_grid     = scan['grid']['mz']
    intens_grid = scan['grid']['intens']
    lmax_mzs    = scan['peaks']['mz']
    lmax_intens = scan['peaks']['intens']
    lmax_idxs   = scan['peaks']['indexes']

    if 'revintens' in algos:
        mzs, intenss, idxs = prune_with_intensity(scan, snr=snr)
        output['revintens'] = {'mz': mzs, 'intens': intenss}
    if 'conv' in algos:
        mzs, intenss, idxs = prune_with_convolution(lmax_mzs, lmax_intens, lmax_idxs, mz_grid, intens_grid, snr=snr)
        output['conv'] = {'mz': mzs, 'intens': intenss}
    if 'grad' in algos:
        mzs, intenss, idxs = prune_with_gradient(lmax_mzs, lmax_intens, lmax_idxs, snr=snr)
        output['grad'] = {'mz': mzs, 'intens': intenss}

    # if ('isograd' in algos) or ('isoconv' in algos):
    #     isotope_scored_intens = apply_peak_isotope_score(lmax_idxs, lmax_intens, mz_grid)
    # if 'isoconv' in algos:
    #     mzs, intenss, idxs = prune_with_convolution(lmax_mzs, isotope_scored_intens, lmax_idxs, mz_grid, intens_grid)
    #     output['isoconv'] = dict(scan, mz=mzs, intens=intenss)
    # if 'isograd' in algos:
    #     mzs, intenss, idxs = prune_with_gradient(lmax_mzs, isotope_scored_intens, lmax_idxs)
    #     output['isograd'] = dict(scan, mz=mzs, intens=intenss)

    if plot:
        plot_lines_and_peaks(
            {'type': 'line',  'x': mz_grid,  'y': intens_grid, 'label': 'Profile'},
            {'type': 'peaks', 'x': lmax_mzs, 'y': lmax_intens, 'label': 'Local Max Peaks'},
            *[{'type': 'peaks', 'x': algo['mz'], 'y': algo['intens'], 'label': label} for label, algo in output.items()]
        )

    return output
