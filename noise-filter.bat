@echo on

:: ----- CDifficile -----
set "MASS_SPECTRA_FILES=E:\Mass Spectra\CDifficile delta\Converted\cdifficile.centroid-vendor.byspec2;E:\Mass Spectra\CDifficile delta\Converted\CDifficile delta-16 20kD band 8thFeb16.byspec2"
jupyter nbconvert --ExecutePreprocessor.timeout=600 --to html --no-input --output="Noise Filter - CDifficile.html" --execute "Batch Noise Filtering.ipynb"


:: ----- iPGR2012 -----
set "MASS_SPECTRA_FILES=E:\Mass Spectra\iPRG2012\Converted\iPRG2012.centroid-vendor.byspec2;E:\Mass Spectra\iPRG2012\Converted\iPRG2012.byspec2"
jupyter nbconvert --ExecutePreprocessor.timeout=600 --to html --no-input --output="Noise Filter - iPRG2012.html" --execute "Batch Noise Filtering.ipynb"
 
 
:: ----- MAM\1448 -----
set "MASS_SPECTRA_FILES=E:\Mass Spectra\MAM\1448\Converted\1448.centroid-vendor.byspec2;E:\Mass Spectra\MAM\1448\Converted\1448.REF_MSMS.byspec2"
jupyter nbconvert --ExecutePreprocessor.timeout=600 --to html --no-input --output="Noise Filter - MAM1448.html" --execute "Batch Noise Filtering.ipynb"


:: ----- MAM\4332 -----
set "MASS_SPECTRA_FILES=E:\Mass Spectra\MAM\4332\Converted\4332.centroid-vendor.byspec2;E:\Mass Spectra\MAM\4332\Converted\4332.REF_MSMS.byspec2"
jupyter nbconvert --ExecutePreprocessor.timeout=600 --to html --no-input --output="Noise Filter - MAM4332.html" --execute "Batch Noise Filtering.ipynb"


:: ----- MAM\6544 -----
set "MASS_SPECTRA_FILES=E:\Mass Spectra\MAM\6544\Converted\6544.centroid-vendor.byspec2;E:\Mass Spectra\MAM\6544\Converted\6544.REF_MSMS.byspec2"
jupyter nbconvert --ExecutePreprocessor.timeout=600 --to html --no-input --output="Noise Filter - MAM6544.html" --execute "Batch Noise Filtering.ipynb"


:: ----- MAM\8292 -----
set "MASS_SPECTRA_FILES=E:\Mass Spectra\MAM\8292\Converted\8292.centroid-vendor.byspec2;E:\Mass Spectra\MAM\8292\Converted\8292.byspec2"
jupyter nbconvert --ExecutePreprocessor.timeout=600 --to html --no-input --output="Noise Filter - MAM8292.html" --execute "Batch Noise Filtering.ipynb"
